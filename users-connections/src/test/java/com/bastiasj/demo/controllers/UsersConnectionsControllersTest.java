package com.bastiasj.demo.controllers;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;

import com.bastiasj.demo.entities.Connection;
import com.bastiasj.demo.entities.User;
import com.bastiasj.demo.services.UsersConnectionsService;

public class UsersConnectionsControllersTest {

	private static UsersConnectionsService usService;
	private static BindingResult mockedBindingResult;
	private static Model mockedModel;
	private static UsersConnectionsControllers usersConnectionsControllers;
	private static User user;
	private static Connection connection;

	@BeforeClass
	public static void setUpUsersConnectionsControllerInstance() {

		usService = mock(UsersConnectionsService.class);
		mockedBindingResult = mock(BindingResult.class);
		mockedModel = mock(Model.class);
		usersConnectionsControllers = new UsersConnectionsControllers(usService);
		user = new User("Blanca");
		connection = new Connection();
	}

	@Test
	public void testAddUserError() throws Exception {

		when(mockedBindingResult.hasErrors()).thenReturn(true);
		assertThat(usersConnectionsControllers.addUser(user, mockedBindingResult, mockedModel)).isEqualTo("add-user");

	}

	@Test
	public void testAddUser() throws Exception {

		when(mockedBindingResult.hasErrors()).thenReturn(false);
		assertThat(usersConnectionsControllers.addUser(user, mockedBindingResult, mockedModel)).isEqualTo("index");

	}

	@Test
	public void testNewUser() throws Exception {
		assertThat(usersConnectionsControllers.newUser(user)).isEqualTo("add-user");
	}

	@Test
	public void testDeleteUser() throws Exception {

		when(usService.findUserById(1l)).thenReturn(Optional.of(new User()));
		assertThat(usersConnectionsControllers.deleteUser(1l, mockedModel)).isEqualTo("index");
	}

	@Test
	public void testAddConnectionError() throws Exception {
		when(mockedBindingResult.hasErrors()).thenReturn(true);
		assertThat(usersConnectionsControllers.addConnection(connection, mockedBindingResult, mockedModel))
				.isEqualTo("add-connection");
	}

	@Test
	public void testAddConnection() throws Exception {
		when(mockedBindingResult.hasErrors()).thenReturn(false);
		assertThat(usersConnectionsControllers.addConnection(connection, mockedBindingResult, mockedModel))
				.isEqualTo("index");
	}

	@Test
	public void testNewConnection() throws Exception {
		assertThat(usersConnectionsControllers.newConnection(connection, mockedModel)).isEqualTo("add-connection");

	}

	@Test
	public void testDeleteConnection() throws Exception {
		when(usService.findConnectionById(1l)).thenReturn(Optional.of(new Connection()));
		assertThat(usersConnectionsControllers.deleteConnection(1l, mockedModel)).isEqualTo("index");
	}

	@Test
	public void testSearch() throws Exception {
		assertThat(usersConnectionsControllers.search(new User(), mockedBindingResult, mockedModel)).isEqualTo("index");
	}

}
