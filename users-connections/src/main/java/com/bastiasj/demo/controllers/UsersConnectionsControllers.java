package com.bastiasj.demo.controllers;

import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.bastiasj.demo.entities.Connection;
import com.bastiasj.demo.entities.User;
import com.bastiasj.demo.services.UsersConnectionsService;

/** @author bastias */
@Controller
public class UsersConnectionsControllers {

	private static final String INDEX = "index";
	private static final String ADD_USER = "add-user";
	private static final String ADD_CONNECTION = "add-connection";

	private Logger logger = LoggerFactory.getLogger(UsersConnectionsControllers.class);

	private UsersConnectionsService usService;

	@Autowired
	public UsersConnectionsControllers(UsersConnectionsService usService) {
		this.usService = usService;
	}

	/**
	 * Method to add a new user.
	 * 
	 * @param user: user object
	 * @param result: binding result
	 * @param model: spring ui model
	 * @return URL to home page.
	 */
	@PostMapping("/adduser")
	public String addUser(@Valid User user, BindingResult result, Model model) {
		if (result.hasErrors()) {
			return ADD_USER;
		}
		usService.addUser(user);
		return returnIndex(model);
	}

	/**
	 * Method to open a new form to create new user.
	 * 
	 * @param user: User object.
	 * @return URL to new user form.
	 */
	@GetMapping("/newuser")
	public String newUser(User user) {
		return ADD_USER;
	}

	/**
	 * Method to delete a user.
	 * 
	 * @param id: user id
	 * @param model: spring ui model
	 * @return URL to home page.
	 */
	@GetMapping("/deleteuser/{id}")
	public String deleteUser(@PathVariable("id") Long id, Model model) {

		logger.debug("Deleting user with id {}", id);

		User user = usService.findUserById(id).orElseThrow(() -> new IllegalArgumentException("Invalid User Id:" + id));
		usService.deleteUser(user);
		return returnIndex(model);
	}

	/**
	 * Method to add a new connection.
	 * 
	 * @param connection: connection object with 2 users.
	 * @param result: binding result
	 * @param model: spring ui model
	 * @return URL to home page.
	 */
	@PostMapping("/addconnection")
	public String addConnection(@Valid Connection connection, BindingResult result, Model model) {
		if (result.hasErrors()) {
			model.addAttribute("allUsers", usService.findAllUsers());
			return ADD_CONNECTION;
		}
		usService.addConnection(connection);
		return returnIndex(model);
	}

	/**
	 * Method to open a new form to create a new connection.
	 * 
	 * @param connection: Connection object.
	 * @param model: spring ui model
	 * @return URL to new connection form.
	 */
	@GetMapping("/newconnection")
	public String newConnection(Connection connection, Model model) {
		model.addAttribute("allUsers", usService.findAllUsers());
		return ADD_CONNECTION;
	}

	/**
	 * Method to delete a connection.
	 * 
	 * @param id: connection id
	 * @param model: spring ui model
	 * @return URL to home page.
	 */
	@GetMapping("/deleteconnection/{id}")
	public String deleteConnection(@PathVariable("id") Long id, Model model) {

		logger.debug("Deleting connection with id {}", id);

		Connection connection = usService.findConnectionById(id)
				.orElseThrow(() -> new IllegalArgumentException("Invalid Connection Id:" + id));
		usService.deleteConnection(connection);
		return returnIndex(model);
	}

	/**
	 * Method to search all connections linked to a particular user.
	 * 
	 * @param userToSearch: user to search
	 * @param result: binding result
	 * @param model: spring ui model
	 * @return URL to home page.
	 */
	@PostMapping("/search")
	public String search(@Valid User userToSearch, BindingResult result, Model model) {

		String userName = userToSearch.getName();
		logger.debug("Searching user with name {}", userName);

		model.addAttribute("users", usService.findAllUsers());
		model.addAttribute("connections",
				StringUtils.isNotBlank(userName) ? usService.findConnectionByUserName(userName)
						: usService.findAllConnection());
		model.addAttribute("userToSearch", new User());
		return INDEX;
	}

	/**
	 * Method to refresh all lists.
	 * 
	 * @param model
	 * @return URL to home page.
	 */
	@GetMapping("/returnindex")
	private String returnIndex(Model model) {
		model.addAttribute("users", usService.findAllUsers());
		model.addAttribute("connections", usService.findAllConnection());
		model.addAttribute("userToSearch", new User());
		return INDEX;
	}

}
