package com.bastiasj.demo.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bastiasj.demo.entities.Connection;
import com.bastiasj.demo.entities.User;
import com.bastiasj.demo.repositories.ConnectionsRepository;
import com.bastiasj.demo.repositories.UsersRepository;

@Service
public class UsersConnectionsService {

	@Autowired
	private UsersRepository usRepository;

	@Autowired
	private ConnectionsRepository connRepository;

	public void addUser(User user) {
		usRepository.save(user);
	}

	public Iterable<User> findAllUsers() {
		return usRepository.findAll();
	}

	public void addConnection(Connection connection) {
		connRepository.save(connection);
	}

	public Iterable<Connection> findAllConnection() {
		return connRepository.findAll();
	}

	public Iterable<Connection> findConnectionByUserName(String name) {
		return connRepository.findConnectionByUserAName(name);
	}

	public Optional<User> findUserById(Long id) {
		return usRepository.findById(id);
	}

	public void deleteUser(User user) {
		usRepository.delete(user);
	}

	public Optional<Connection> findConnectionById(Long id) {
		return connRepository.findById(id);
	}

	public void deleteConnection(Connection connection) {
		connRepository.delete(connection);
	}

}
