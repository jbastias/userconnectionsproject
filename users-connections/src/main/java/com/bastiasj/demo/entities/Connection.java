package com.bastiasj.demo.entities;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

@Entity
public class Connection {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@NotNull(message = "User A is mandatory")
	@ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	@JoinColumn(name = "user_a_id", referencedColumnName = "id")
	private User userA;

	@NotNull(message = "User B is mandatory")
	@ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	@JoinColumn(name = "user_b_id", referencedColumnName = "id")
	private User userB;

	public Connection() {
		super();
	}

	public Connection(@NotNull(message = "User A is mandatory") User userA,
			@NotNull(message = "User B is mandatory") User userB) {
		super();
		this.userA = userA;
		this.userB = userB;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public User getUserA() {
		return userA;
	}

	public void setUserA(User userA) {
		this.userA = userA;
	}

	public User getUserB() {
		return userB;
	}

	public void setUserB(User userB) {
		this.userB = userB;
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(userA).append(userB).toHashCode();
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof Connection)) {
			return false;
		}
		Connection castOther = (Connection) other;
		return new EqualsBuilder().append(id, castOther.id).append(userA, castOther.userA)
				.append(userB, castOther.userB).isEquals();
	}

}
