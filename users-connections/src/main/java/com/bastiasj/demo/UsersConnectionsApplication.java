package com.bastiasj.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan(basePackages = { "com.bastiasj.demo" })
@EnableJpaRepositories(basePackages = "com.bastiasj.demo.repositories")
@EnableTransactionManagement
@EntityScan(basePackages = "com.bastiasj.demo.entities")
public class UsersConnectionsApplication {

	public static void main(String[] args) {
		SpringApplication.run(UsersConnectionsApplication.class, args);
	}

}
