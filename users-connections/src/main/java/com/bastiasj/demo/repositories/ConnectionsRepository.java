package com.bastiasj.demo.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.bastiasj.demo.entities.Connection;

@Repository
public interface ConnectionsRepository extends CrudRepository<Connection, Long> {

	@Query("SELECT c FROM Connection c, User u WHERE u.name = ?1 AND (c.userA.id = u.id OR c.userB.id = u.id)")
	public List<Connection> findConnectionByUserAName(String name);

}
