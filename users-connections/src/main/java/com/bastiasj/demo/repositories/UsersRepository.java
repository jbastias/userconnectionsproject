package com.bastiasj.demo.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.bastiasj.demo.entities.User;

@Repository
public interface UsersRepository extends CrudRepository<User, Long> {

}
